import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentPage = 0;

  checkWindowIndex(value:number){
    if (this.currentPage < 2){
      return (value < 5);
    }
    if (this.currentPage > this.images.length - 3){
      return (value > this.images.length - 6);
    }
    return (Math.abs(this.currentPage - value) < 3);
  }

  images = [
    {
      title: 'At the Beach',
      url: 'https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Beneath the castle',
      url: 'https://images.unsplash.com/photo-1544939514-aa98d908bc47?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'In the woods',
      url: 'https://images.unsplash.com/photo-1441974231531-c6227db76b6e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'In the desert',
      url: 'https://images.unsplash.com/photo-1509316785289-025f5b846b35?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Busan, Korea',
      url: 'https://images.unsplash.com/photo-1596785233307-858cefe13b43?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Say "Hi" to Teddy',
      url: 'https://images.unsplash.com/photo-1541560584704-f2767960951a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Beware of infection',
      url: 'https://images.unsplash.com/photo-1549231482-5cf39d19fba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'So sad',
      url: 'https://images.unsplash.com/photo-1585245802021-88032c9350c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'WTF is she doing?',
      url: 'https://images.unsplash.com/photo-1580855344331-10eaa5565fc8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'The roof is on fire',
      url: 'https://images.unsplash.com/photo-1455747634646-0ef67dfca23f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Vacancy',
      url: 'https://images.unsplash.com/photo-1532140225690-f6d25ab4c891?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Repentance',
      url: 'https://images.unsplash.com/photo-1534451440277-f727336850c2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Leviathan',
      url: 'https://images.unsplash.com/photo-1551431063-b02c0e9cd2bb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Behemoth',
      url: 'https://images.unsplash.com/photo-1574140382911-098171183d8b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: "What's up, Pussycat?",
      url: 'https://images.unsplash.com/photo-1527720175429-214744972b4b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'War - hu - what is it good for?',
      url: 'https://images.unsplash.com/photo-1561323577-5b76286cb15f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
    },
    {
      title: 'Absolutely nothing! - Say it again!',
      url: 'https://images.unsplash.com/photo-1418489614040-ee0ac582072e?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
    },
  ];
}
