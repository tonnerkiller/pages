import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';

@Directive({
  selector: '[appTimes]'
})
export class TimesDirective {

  constructor(
    private viewContainer: ViewContainerRef,
    private tempplateRef: TemplateRef<any>
  ){  }

  @Input('appTimes') set render(times: number) {
    // delete
    this.viewContainer.clear();
    // recreate
    for (let i =0; i < times; i++){
      this.viewContainer.createEmbeddedView(this.tempplateRef, {
        index: i
      });
    }
  }

}
